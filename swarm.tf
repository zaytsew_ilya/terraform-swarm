variable "do_token" {}
variable "instance_count" {}
variable "cluster_name" {
  type = "string"
  default = "test"
}
variable "ssh_public_key" {}
variable "ssh_private_key" {}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_ssh_key" "cluster-key" {
  name       = "${var.cluster_name}-public-key"
  public_key = "${file("${var.ssh_public_key}")}"
}

resource "digitalocean_droplet" "master" {
  name = "${var.cluster_name}-master"
  region = "ams3"
  size = "s-1vcpu-1gb"
  image = "ubuntu-18-04-x64"
  ssh_keys = ["${digitalocean_ssh_key.cluster-key.fingerprint}"]

  provisioner "local-exec" {
    command = "DEBUG=true DAYS=1825 HOST=${digitalocean_droplet.master.ipv4_address} DIR=certs/${digitalocean_droplet.master.name} sh bin/generate_certs.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /root/docker/certs"
    ] 

    connection {
      type     = "ssh"
      user     = "root"
      host     = "${digitalocean_droplet.master.ipv4_address}"
      private_key = "${file("${var.ssh_private_key}")}"
    }
  }

  provisioner "file" {
    source = "certs/${digitalocean_droplet.master.name}/"
    destination = "/root/docker/certs"

    connection {
      type     = "ssh"
      user     = "root"
      host     = "${digitalocean_droplet.master.ipv4_address}"
      private_key = "${file("${var.ssh_private_key}")}"
    }
  }

  provisioner "remote-exec" {
    script = "bin/docker_install.sh" 

    connection {
      type     = "ssh"
      user     = "root"
      host     = "${digitalocean_droplet.master.ipv4_address}"
      private_key = "${file("${var.ssh_private_key}")}"
    }
  }

  provisioner "local-exec" {
    command = "docker --tlsverify --tlscacert=certs/${digitalocean_droplet.master.name}/ca.pem --tlscert=certs/${digitalocean_droplet.master.name}/cert.pem --tlskey=certs/${digitalocean_droplet.master.name}/key.pem -H=tcp://${digitalocean_droplet.master.ipv4_address}:2376 swarm init --advertise-addr ${digitalocean_droplet.master.ipv4_address}"
  }

  provisioner "local-exec" {
    command = "docker --tlsverify --tlscacert=certs/${digitalocean_droplet.master.name}/ca.pem --tlscert=certs/${digitalocean_droplet.master.name}/cert.pem --tlskey=certs/${digitalocean_droplet.master.name}/key.pem -H=tcp://${digitalocean_droplet.master.ipv4_address}:2376 swarm join-token -q worker > .swarm_token"
  }
}

resource "digitalocean_droplet" "worker" {
  name = "${var.cluster_name}-worker-${count.index + 1}"
  region = "ams3"
  size = "s-1vcpu-1gb"
  image = "ubuntu-18-04-x64"
  ssh_keys = ["${digitalocean_ssh_key.cluster-key.fingerprint}"]
  count = "${var.instance_count}"

  provisioner "local-exec" {
    command = "DEBUG=true DAYS=1825 HOST=${digitalocean_droplet.worker[count.index].ipv4_address} DIR=certs/${digitalocean_droplet.worker[count.index].name} sh bin/generate_certs.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /root/docker/certs"
    ]

    connection {
      type     = "ssh"
      user     = "root"
      host     = "${digitalocean_droplet.worker[count.index].ipv4_address}"
      private_key = "${file("${var.ssh_private_key}")}"
    }
  }

  provisioner "file" {
    source = "certs/${digitalocean_droplet.worker[count.index].name}/"
    destination = "/root/docker/certs"

    connection {
      type     = "ssh"
      user     = "root"
      host     = "${digitalocean_droplet.worker[count.index].ipv4_address}"
      private_key = "${file("${var.ssh_private_key}")}"
    }
  }

  provisioner "remote-exec" {
    script = "bin/docker_install.sh"

    connection {
      type     = "ssh"
      user     = "root"
      host     = "${digitalocean_droplet.worker[count.index].ipv4_address}"
      private_key = "${file("${var.ssh_private_key}")}"
    }
  }

  provisioner "local-exec" {
    command = "docker --tlsverify --tlscacert=certs/${digitalocean_droplet.worker[count.index].name}/ca.pem --tlscert=certs/${digitalocean_droplet.worker[count.index].name}/cert.pem --tlskey=certs/${digitalocean_droplet.worker[count.index].name}/key.pem -H=tcp://${digitalocean_droplet.worker[count.index].ipv4_address}:2376 swarm join --token $(cat .swarm_token) ${digitalocean_droplet.master.ipv4_address}:2377"
  }
}
