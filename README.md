## Terraform swarm ##

### Usage ###

1. Generate ssh keys
    ```
    ssh-keygen -t rsa
    ```

1. Initialize terraform
    ```
    terraform init
    ```

1. Create file `terraform.tfvars` in project directory
    ```
    do_token = "YOUR_TOKEN"  
    cluster_name = "my-cluster"  
    ssh_public_key = "/pass/to/ssh/public/key"  
    ssh_private_key = "/pass/to/ssh/private/key"  
    ```
1. Run   
    ```
    terraform apply
    ```

