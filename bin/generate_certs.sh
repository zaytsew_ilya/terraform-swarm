#!/usr/bin/env bash

if [ "${DEBUG}" = true ]; then
  set -x
  export
  whoami
fi

if [ -d "$DIR" ]; then
  rm -rf $DIR
fi

mkdir $DIR

PASS=$(openssl rand -hex 16)

# generate CA private and public keys
openssl genrsa -aes256 -out $DIR/ca-key.pem -passout pass:$PASS 4096
openssl req -new -x509 -days 1825 -passin pass:$PASS -subj "/CN=$HOST/" -key $DIR/ca-key.pem -sha256 -out $DIR/ca.pem

# create a server key and certificate signing request
openssl genrsa -out $DIR/server-key.pem 4096
openssl req -subj "/CN=$HOST" -sha256 -new -key $DIR/server-key.pem -out $DIR/server.csr

# sign the public key with our CA
echo subjectAltName = DNS:$HOST,IP:$HOST,IP:127.0.0.1 >> $DIR/extfile.cnf
echo extendedKeyUsage = serverAuth >> $DIR/extfile.cnf
openssl x509 -req -days $DAYS -sha256 -passin pass:$PASS -in $DIR/server.csr -CA $DIR/ca.pem -CAkey $DIR/ca-key.pem -CAcreateserial -out $DIR/server-cert.pem -extfile $DIR/extfile.cnf

# create a client key and certificate signing request
openssl genrsa -out $DIR/key.pem 4096
openssl req -subj '/CN=client' -new -key $DIR/key.pem -out $DIR/client.csr -passin pass:$PASS
echo extendedKeyUsage = clientAuth > $DIR/extfile-client.cnf
openssl x509 -req -days $DAYS -sha256 -in $DIR/client.csr -CA $DIR/ca.pem -CAkey $DIR/ca-key.pem -CAcreateserial -out $DIR/cert.pem -extfile $DIR/extfile-client.cnf -passin pass:$PASS

# cleanup
rm -v $DIR/client.csr $DIR/server.csr $DIR/extfile.cnf $DIR/extfile-client.cnf

openssl rsa -in $DIR/server-key.pem -out $DIR/server-key.pem -passin pass:$PASS
openssl rsa -in $DIR/key.pem -out $DIR/key.pem -passin pass:$PASS

chmod -v 0400 $DIR/ca-key.pem $DIR/key.pem $DIR/server-key.pem
chmod -v 0444 $DIR/ca.pem $DIR/server-cert.pem $DIR/cert.pem




